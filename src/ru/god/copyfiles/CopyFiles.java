package ru.god.copyfiles;

import java.util.Scanner;

/**
 * Класс, реализующий выполнение сначала последовательного копирования,
 * а затем параллельного копирования двух файлов с помощью многопоточности
 *
 * @author Горбачева, 16ИТ18к
 */
public class CopyFiles {
    private static final String NUMBER_OF_FILES = "Сколько файлов вы хотите скопировать?";

    private static final String ERROR_OF_NUMBER_OF_FILES = "Введите корректное число (больше нуля)";

    private static final String INFO_ABOUT_COPY = "Файл, расположенный по адресу %s, был успешно скопирован " +
            "в файл, расположенный по адресу %n %s, за %d миллисекунд %n";

    private static final String INFO_ABOUT_ALL_COPY = "Все файлы были успешно скопированы за %d миллисекунд %n";

    private static final String ENTER_PATH_FROM = "Введите полное имя файла, который нужно скопировать";

    private static final String ENTER_PATH_INTO = "Введите полное имя файла, в который нужно скопировать исходник";

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int numberOfFiles = numberOfFiles();
        System.out.printf(INFO_ABOUT_ALL_COPY, lineCopy(numberOfFiles));
        System.out.printf(INFO_ABOUT_ALL_COPY, parallelCopy(numberOfFiles));
    }

    /**
     * Возвращает корректное число файлов, которые нужно скопировать
     *
     * @return число файлов, которые нужно скопировать
     */
    private static int numberOfFiles() {
        System.out.println(NUMBER_OF_FILES);
        int numberOfFiles = scanner.nextInt();
        while (numberOfFiles <= 0) {
            System.out.println(ERROR_OF_NUMBER_OF_FILES);
            numberOfFiles = scanner.nextInt();
        }
        return numberOfFiles;
    }

    /**
     * Возвращает время, за которое последовательно скопировались файлы
     *
     * @param numberOfFiles - количество файлов
     * @return время, за которое последовательно скопировались файлы
     */
    private static long lineCopy(int numberOfFiles) {
        System.out.println("\nПОСЛЕДОВАТЕЛЬНОЕ КОПИРОВАНИЕ\n\n\n");
        ThreadCopy[] arrayThread = arrayThreads(numberOfFiles);
        long beforeAll = System.currentTimeMillis();
        for (int i = 0; i < numberOfFiles; i++) {
            long before = System.currentTimeMillis();
            arrayThread[i].start();
            try {
                arrayThread[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.printf(INFO_ABOUT_COPY, arrayThread[i].getFrom(), arrayThread[i].getInto(), System.currentTimeMillis() - before);
        }
        return System.currentTimeMillis() - beforeAll;
    }

    /**
     * Возвращает массив потоков для копирования
     *
     * @param numberOfFiles - количество файлов
     * @return массив потоков для копирования
     */
    private static ThreadCopy[] arrayThreads(int numberOfFiles) {
        ThreadCopy[] arrayThread = new ThreadCopy[numberOfFiles];
        for (int i = 0; i < numberOfFiles; i++) {
            System.out.println(ENTER_PATH_FROM);
            String source = scanner.nextLine();
            System.out.println(ENTER_PATH_INTO);
            String target = scanner.nextLine();
            arrayThread[i] = new ThreadCopy(source, target);
        }
        return arrayThread;
    }

    /**
     * Возвращает время, за которое параллельно скопировались файлы
     *
     * @param numberOfFiles - количество файлов
     * @return время, за которое параллельно скопировались файлы
     */
    private static long parallelCopy(int numberOfFiles) {
        System.out.println("\nПАРАЛЛЕЛЬНОЕ КОПИРОВАНИЕ\n\n\n");
        ThreadCopy[] arrayThread = arrayThreads(numberOfFiles);
        long beforeAll = System.currentTimeMillis();
        for (int i = 0; i < numberOfFiles; i++) {
            arrayThread[i].start();
        }
        for (int i = 0; i < numberOfFiles; i++) {
            try {
                arrayThread[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return System.currentTimeMillis() - beforeAll;
    }
}
