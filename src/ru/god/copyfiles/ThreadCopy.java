package ru.god.copyfiles;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Класс, позволяющий создавать потоки, выполняющие копирование содержимого одного файла в другой
 *
 * @author Горбачева, 16ИТ18к
 */
public class ThreadCopy extends Thread {

    /**
     * Полное имя файла, который нужно скопировать
     */
    private String from;
    /**
     * Полное имя файла, в который нужно скопировать исходник
     */
    private String into;

    ThreadCopy(String from, String into) {
        this.from = from;
        this.into = into;
    }

    String getFrom() {
        return from;
    }

    String getInto() {
        return into;
    }

    /**
     * Метод, с помощью которого считывается информация из одного файла и записывается в другой
     * Таким образом выполняется копирование
     */
    public void run() {
        List<String> arrayList;
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(into))) {
            arrayList = Files.readAllLines(Paths.get(from));
            for (String anArrayList : arrayList) {
                bufferedWriter.write(anArrayList + "\n");
            }
        } catch (IOException e) {
            System.out.println("Произошла ошибка в процессе копирования, операция не выполнена");
        }
    }
}